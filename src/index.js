import React from 'react';
import ReactDOM from 'react-dom';

const ROOT = <span>MARVEL ...</span>;
ReactDOM.render(ROOT, document.getElementById('root'));
